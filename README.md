# Tuba

This project is a lightweight video streaming server. It transcodes on-the-fly so you don't have to re-encode your entire media collection. It uses video.js which is built to support most modern browsers.

## Installation

Required packages:

* ruby 2.4.1 or later
* ffmpeg 3.3.3 or later

Install dependencies (located in Gemfile) using bundler:

```
bundle install
```

You can run a server using:

```
puma
```

Open a browser to http://localhost:9292 and enjoy.

## Configuration

The `config.yml` file (copy from `config.yml.sample`) contains a list of parameters that configure the server.

### Title

This field changes the name of the server and replaces the default "Tuba" wherever it appears.

```
title: "My Media"
```

### Host and Port

This will update how the web server starts.

```
host: 0.0.0.0
port: 8001
```

### Paths

Here is where you point the different categories to your local files.

```
paths:
  shows: /mnt/data/shows
  movies: /mnt/data/movies
```

## Open Source Attribution

We use the following open source libraries:

For the frontend:

* video.js: [site](http://videojs.com/), [code](https://github.com/videojs), Apache 2.0
* videojs-contrib-hls: [code](https://github.com/videojs/videojs-contrib-hls), Apache 2.0
* videojs-flash: [code](https://github.com/videojs/videojs-flash), Apache 2.0
* videojs-overlay: [code](https://github.com/brightcove/videojs-overlay), Apache 2.0

For the backend:

* ruby: Scripting Language: [site](https://www.ruby-lang.org/en/), [code](https://github.com/ruby/ruby), BSD 2-Clause
* rack: Ruby HTTP Framework: [site](http://rack.github.io/), [code](https://github.com/rack/rack), MIT
* sinatra: Ruby Web Framework: [site](http://www.sinatrarb.com/), [code](https://github.com/sinatra/sinatra), MIT
* puma: Concurrent Web Server: [site](http://puma.io/), [code](https://github.com/puma/puma), BSD 3-Clause "New"
* sass: CSS Framework: [site](http://sass-lang.com/), [code](https://github.com/sass/sass), MIT
* haml: HTML Framework: [site](http://haml.info/), [code](https://github.com/haml/haml), MIT
* i18n: Internationalization Library: [site](http://guides.rubyonrails.org/i18n.html), [code](https://github.com/svenfuchs/i18n), MIT

## Contributing

The following are accepted and cherished forms of contribution:

* Filing a bug issue. (We like to see feature requests and just normal bugs)
* Fixing a bug. (It's obviously helpful!)
* Adding documentation. (Help us with our docs or send us a link to your blog post!)
* Adding features.
* Adding artwork. (Art is the true visual form of professionalism)

The following are a bit harder to really accept, in spite of the obvious effort that may go into them, so please avoid this:

* Changing all of the javascript to coffeescript because it is "better"
* Rewriting all of the sass to whatever is newer. (It's happened to me before)
* Porting everything to rails.
* Creating a pull request with a "better" software license.

In general, contributions are easily provided by doing one of the following:

* Fork and clone the project.
* Update the code on your end however you see fit.
* Push that code to a public server.
* Create a pull request from your copy to ours.

The above is the most convenient process. You may create an issue with a link to a repository or tar/zip containing your code or patches as well, if git is not your thing.

## Code of Conduct

Please note that this project is released with a [Contributor Code of Conduct](CODE_OF_CONDUCT.md). By participating in this project you agree to abide by its terms.

## License

Tuba is licensed under the AGPL 2.0. Refer to the [LICENSE.txt](LICENSE.txt) file in the root of the repository for specific details.
