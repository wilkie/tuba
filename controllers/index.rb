# Tuba - Video Streaming Web Server
# Copyright (C) 2017 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

class Application
  get '/' do
    render :haml, :index, :locals => {}
  end

  get '/watch' do
    render :haml, :watch, :locals => {}
  end

  get '/movies' do
    render :haml, :"movies/index", :locals => {}
  end

  get '/movies/:name' do
    if not Application::Movie.exists?(params[:name])
      status 404
      return
    end

    movie = Application::Movie.new(:name => params[:name])

    render :haml, :"movies/view", :locals => {:movie => movie, :up => Application::Movie.url}
  end

  get '/shows' do
    render :haml, :"shows/index", :locals => {}
  end

  get '/shows/:name' do
    if not Application::Show.exists?(params[:name])
      status 404
      return
    end

    show = Application::Show.new(:name => params[:name])

    render :haml, :"shows/view", :locals => {:show => show, :up => Application::Show.url}
  end

  get '/shows/:name/:season' do
    if not Application::Show.exists?(params[:name])
      status 404
      return
    end

    show   = Application::Show.new(:name => params[:name])
    season = Application::Show::Season.new(:show => show, :name => params[:season])

    render :haml, :"shows/seasons/view", :locals => {:show => show, :season => season, :up => show.url}
  end

  get '/shows/:name/:season/:episode' do
    if not Application::Show.exists?(params[:name])
      status 404
      return
    end

    show    = Application::Show.new(:name => params[:name])
    season  = Application::Show::Season.new(:show => show, :name => params[:season])
    episode = Application::Show::Episode.new(:show => show, :season => season, :name => params[:episode])

    render :haml, :"shows/episodes/view", :locals => {:show => show, :season => season, :episode => episode, :up => season.url}
  end

  # HLS stream start
  get '/shows/:name/:season/:episode/hls' do
    if not Application::Show.exists?(params[:name])
      status 404
      return
    end

    show    = Application::Show.new(:name => params[:name])
    season  = Application::Show::Season.new(:show => show,
                                            :name => params[:season])
    episode = Application::Show::Episode.new(:show   => show,
                                             :season => season,
                                             :name   => params[:episode])

    # Look for stream start
    startTime = params["start"]
    puts "Starting at #{startTime}"

    filePath = episode.path

    transcoder = Application::Transcoder.new(filePath)
    transcoder.start(startTime)

    # Redirect to m3u8 when it is available
    until File.exists?(transcoder.m3u8StreamPath)
    end

    {
      :streamURL => transcoder.m3u8StreamURL,
      :uuid      => transcoder.uuid
    }.to_json
  end

  # Retrieves the metadata about the given video
  get '/shows/:name/:season/:episode/metadata' do
    if not Application::Show.exists?(params[:name])
      status 404
      return
    end

    show    = Application::Show.new(:name => params[:name])
    season  = Application::Show::Season.new(:show => show,
                                            :name => params[:season])
    episode = Application::Show::Episode.new(:show   => show,
                                             :season => season,
                                             :name   => params[:episode])

    filePath = episode.path

    transcoder = Application::Transcoder.new(filePath)
    transcoder.metadata.to_json
  end

  # Retrieves a jpeg of a frame of the episode
  get '/shows/:name/:season/:episode/frame' do
    if not Application::Show.exists?(params[:name])
      status 404
      return
    end

    show    = Application::Show.new(:name => params[:name])
    season  = Application::Show::Season.new(:show => show,
                                            :name => params[:season])
    episode = Application::Show::Episode.new(:show   => show,
                                             :season => season,
                                             :name   => params[:episode])

    filePath = episode.path

    transcoder = Application::Transcoder.new(filePath)

    content_type "image/jpg"

    stream :keep_open do |out|
      transcoder.frame(:start => (params["start"] || 50)) do |data|
        if data.nil? || data.length == 0
        end

        out.write(data)
      end
    end
  end

  # HLS Segment
  get '/hls/segments/:uuid/:segment' do
    transcoder = Application::Transcoder.open(params[:uuid])

    if transcoder.nil?
      status 404
      return
    end

    # Get segment path and return segment data
    segmentPath = transcoder.ping(params[:segment])
    if segmentPath.nil?
      status 404
      return
    end

    send_file segmentPath
  end
end
