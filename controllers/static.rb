# Tuba - Video Streaming Web Server
# Copyright (C) 2017 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

class Application
  # 404 route
  not_found do
    markdown :"static/404", :layout => :static
  end

  # 500 route
  error do
    markdown :"static/500", :layout => :static
  end

  # Successfully get the 404 page
  get '/404' do
    markdown :"static/404", :layout => :static
  end

  # Successfully get the 500 page
  get '/500' do
    markdown :"static/500", :layout => :static
  end
end
