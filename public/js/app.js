window.Application = {}
window.Application.Util = {}

function getParameterByName(name, url) {
  if (!url) url = window.location.href;
  name = name.replace(/[\[\]]/g, "\\$&");
  var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
    results = regex.exec(url);
  if (!results) return null;
  if (!results[2]) return '';
  return decodeURIComponent(results[2].replace(/\+/g, " "));
}

window.Application.Util.ajax = function(method, url, data, callback, responseType) {
  var oReq = new XMLHttpRequest();

  if (callback) {
    oReq.addEventListener("load", function(event) {
      if (oReq.readyState == 4 && oReq.status == 200) {
        var response = oReq.responseText;

        if (responseType == "application/json" || responseType == "json") {
          response = JSON.parse(response);
        }

        callback(response);
      }
    });
  }

  oReq.open(method, url);
  oReq.setRequestHeader('X-Requested-With', 'XMLHttpRequest');

  if (responseType == "json" || responseType == "application/json") {
    oReq.setRequestHeader('Accept', 'application/json');
  }

  if (data) {
    oReq.send(data);
  }
  else {
    oReq.send();
  }

  return oReq;
};

window.Application.Util.post = function(url, data, callback, responseType) {
  var formdata = undefined;

  if (data) {
    formdata = new FormData();
    Object.keys(data).forEach(function(key) {
      formdata.set(key, data[key]);
    });
  }

  return window.Application.Util.ajax("POST", url, formdata, callback, responseType);
}

window.Application.Util.get = function(url, callback, responseType) {
  return window.Application.Util.ajax("GET", url, null, callback, responseType);
}

window.addEventListener("load", function(event) {
  if (document.querySelector("#stream")) {
    console.log("Streaming video");

    var hlsURL = window.location.href + "/hls";
    var metadataURL = window.location.href + "/metadata";
    var startTime = parseFloat(getParameterByName('start') || "0.0");
    var loading = -1;

    var streamVideo = function(url, start) {
      if (loading == start) {
        return;
      }

      loading = start;

      // Fake the current time
      (function() {
        var Player = videojs.getComponent("Player");
        if (!Player.prototype.oldCurrentTime) {
          Player.prototype.oldCurrentTime = Player.prototype.currentTime;
        }
        Player.prototype.currentTime = function(seconds) {
          if (typeof seconds !== 'undefined') {
            // In reality, we need to switch our source and re-stream
            streamVideo(url, seconds);
            return;
          }

          this.oldCurrentTime();
          this.cache_.currentTime += start;
          return this.cache_.currentTime;
        };
      })();

      // Gather Metadata
      Application.Util.get(metadataURL, function(metadata) {
        // Replace the duration function internally in video.js
        (function() {
          var Player = videojs.getComponent("Player");
          var oldDuration = Player.prototype.duration;
          Player.prototype.duration = function(seconds) {
            if (typeof seconds !== 'undefined') {
              // In reality, we need to switch our source and restream
              oldDuration.call(this, metadata.duration);
              return;
            }

            return metadata.duration;
          };
        })();

        var player = videojs('stream');
        player.controls(true);
        player.pause();

        var banner = "<div class='banner vjs-control-bar'>VIDEO TITLE</div>";

        player.overlay({
          overlays: [{
            "start": "playing",
            "end":   3,
            "align": "top",
            "content": banner
          },{
            "start": "pause",
            "end":   "play",
            "align": "top",
            "content": banner
          }]
        });

        // Start a transcoding and retrieve the HLS stream file location
        Application.Util.get(hlsURL + "?start=" + start, function(streamInfo) {
          loading = -1;

          player.src({
            type: "application/x-mpegURL",
            src:  streamInfo.streamURL
          });

          player.play();
        }, "json");
      }, "json");
    };

    streamVideo(hlsURL, startTime);
  }
});
