# Tuba - Video Streaming Web Server
# Copyright (C) 2017 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

class Application
  class Show
    class Episode
      attr_reader :name
      attr_reader :season
      attr_reader :show

      def initialize(options = {})
        @name = options[:name] || "Unknown"
        @subpath = options[:path] || @name
        @season = options[:season]
        @show = options[:show]
      end

      def path
        # Figure out the file extension
        baseFilePath = File.join(self.season.path, @subpath)

        ["mkv", "mp4", "avi"].each do |ext|
          currentPath = "#{baseFilePath}.#{ext}"

          return currentPath if File.exists?(currentPath)
        end

        return nil
      end

      def url
        "#{self.season.url}/#{self.name}"
      end
    end
  end
end
