# Tuba - Video Streaming Web Server
# Copyright (C) 2017 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

class Application
  class Show
    attr_reader :name
    attr_reader :path

    # The path to shows
    def self.path
      Application::Config.options["paths"]["shows"]
    end

    # The main url for all shows
    def self.url
      "/shows"
    end

    # Produce a list of shows
    def self.list
      Dir.entries(Show.path).select{|e| File.directory?(File.join(Show.path, e)) && !(e == '.' || e == '..')}.sort.map do |path|
        path = File.join(Show.path, path)
        Show.new(:path => path, :name => File.basename(path))
      end
    end

    def self.pathFor(name)
      File.realpath(File.join(Show.path, name))
    end

    def self.exists?(name)
      path = self.pathFor(name)

      path.start_with?(Show.path) && File.exists?(path)
    end

    def initialize(options = {})
      @name = options[:name] || "unknown"
      @path = options[:path]

      if @path.nil?
        if Show.exists?(@name)
          @path = Show.pathFor(@name)
        end
      end
    end

    def seasons(options = {})
      # List directories show path
      Dir.entries(self.path).select{|e| File.directory?(File.join(self.path, e)) && !(e == '.' || e == '..')}.sort.map do |path|
        Show::Season.new(:path => path, :name => path, :show => self)
      end
    end

    def url
      "/shows/#{self.name}"
    end
  end
end
