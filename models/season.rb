# Tuba - Video Streaming Web Server
# Copyright (C) 2017 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

class Application
  class Show
    class Season
      attr_reader :name
      attr_reader :show

      def initialize(options = {})
        @name = options[:name] || "Unknown"
        @subpath = options[:path] || @name
        @show = options[:show]
      end

      def path
        File.join(self.show.path, @subpath)
      end

      # Lists the episodes inside the season
      def list(options = {})
        Dir.entries(self.path).select{|e| File.file?(File.join(self.path, e))}.sort.map do |path|
          Show::Episode.new(:path => path, :name => File.basename(path, File.extname(path)), :season => self, :show => self.show)
        end
      end

      def url
        "#{self.show.url}/#{self.name}"
      end
    end
  end
end
