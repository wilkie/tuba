# Tuba - Video Streaming Web Server
# Copyright (C) 2017 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

class Application
  module Config
    require 'yaml'

    def self.defaults
      {
        "title" => "Tuba"
      }
    end

    def self.samplePath
      File.join(File.dirname(__FILE__), "..", 'config.yml.sample')
    end

    def self.path
      File.join(File.dirname(__FILE__), "..", 'config.yml')
    end

    def self.load
      # If config.yml does not exist, create it using config.yml.sample
      if not File.exists?(self.path) && File.exists?(self.samplePath)
        require 'fileutils'
        FileUtils.cp(self.samplePath, self.path)
      end

      # Parse the configuration
      YAML::load(File.open(self.path, "r"))
    end

    def self.options
      if !defined?(@@options)
        @@options = self.load

        # Default title
        self.defaults.each do |k, v|
          @@options[k] ||= v
        end
      end

      @@options
    end
  end
end
