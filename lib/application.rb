# Tuba - Video Streaming Web Server
# Copyright (C) 2017 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

require 'bundler'
Bundler::require

require 'sinatra/streaming'

# This is the base application class for the server.
class Application < Sinatra::Base
  # Use root directory as root
  set :app_file => '.'
  set :server, :puma

  # HAML
  #
  # Use HTML5
  set :haml, :format => :html5

  # Ensure we use a chunk-encoding
  helpers Sinatra::Streaming

  # SASS
  require 'sass/plugin/rack'
  Sass::Plugin.options[:template_location] = "./views/stylesheets"
  Sass::Plugin.options[:css_location]      = "./public/stylesheets"
  use Sass::Plugin::Rack

  # I18n
  I18n.load_path += Dir[File.join(File.dirname(__FILE__), "..", 'config', 'locales', '*.yml')]
  I18n.load_path += Dir[File.join(Gem::Specification.find_by_name('rails-i18n').gem_dir, 'rails', 'locale', '*.yml')]

  use Rack::MethodOverride

  # Reloader
  configure :development do
    puts "***** In Development Mode *****"
    puts "Using Reloader"

    register Sinatra::Reloader

    %w(models).each do |dir|
      Dir[File.join(File.dirname(__FILE__), "..", dir, '*.rb')].each do |file|
        also_reload file
      end
    end

    puts "*******************************"
  end

  # Gather routes
  require_relative '../controllers/index'

  # Gather models
  require_relative '../models/show'
  require_relative '../models/season'
  require_relative '../models/episode'
  require_relative '../models/movie'
end

require_relative 'stream'
require_relative 'config'
require_relative 'transcoder'
