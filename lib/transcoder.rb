# Tuba - Video Streaming Web Server
# Copyright (C) 2017 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

class Application
  # Hooks to ffmpeg to transcode video streams to various formats.
  #
  # The way the transcoder works is that it assigns a UUID to any new transcoding
  # session. Each session can start a transcode to a particular format and then
  # that session has an active process id (assigned to @stream) for ffmpeg.
  #
  # For HLS encoding, ffmpeg will create stream segments inside the streams path
  # at the uuid plus filenames formated as segment_%05d.ts. Segments are
  # incremental such that the first is segment_00000.ts and so on regardless of
  # where the stream starts (such as seeking to the middle of a video.)
  #
  # Normally, ffmpeg uses the entire CPU and transcodes as fast as possible. To
  # offset this, the script checks, upon seeing ffmpeg write to stderr which
  # is indicative that it performed work, for a new segment file. If it sees
  # that there is a surplus of segments that have not been retrieved by a
  # client, it pauses the ffmpeg process. (SIGSTOP) It keeps track of the
  # available segments it has transcoded as @segmentsAvailable.
  #
  # On the other side, there is a method #ping which gives the segment file
  # requested by the client. The segment file is parsed to determine the
  # segment number and it always resumes the ffmpeg process. (SIGCONT) The
  # last segment retrieved is @segmentsConsumed, which is maintained to be
  # whatever the largest value it has previously seen.
  #
  # When videos are seeked, this simply starts a new transcoding process and
  # kills the old. The simple way of doing that is to just create a new
  # instance of the Transcoder. This will yield a new UUID for the stream
  # and the video player simply changes its src to the new hls m3u8 playlist.
  #
  # Normal transcoding, to say a format like mp4 or ogg, is done all at once.
  # It is possible that it can be slowed down to stream as well, although
  # often, the entire stream is expected in a video player. Currently, such
  # transcoding to these formats is done for download capabilitity.
  class Transcoder
    require 'open3'
    require 'json'
    require 'securerandom'

    attr_reader :path
    attr_reader :uuid

    def self.open(uuid)
      if !defined?(@@streamsByUUID)
        @@streamsByUUID = {}
      end

      @@streamsByUUID[uuid]
    end

    def initialize(filePath)
      @uuid = SecureRandom.uuid

      @maxSegments = 10
      @segmentSize = 3

      @path = filePath
      @metadata = nil
      @streaming = false
      @stream = nil

      @segmentsAvailable = 0
      @segmentsConsumed  = 0
      @segmentMutex = Mutex.new

      if !defined?(@@streamsByUUID)
        @@streamsByUUID = {}
      end

      @@streamsByUUID[@uuid] = self
    end

    def purge
      # Delete out stream segments
      require 'fileutils'
      FileUtils.rm_r self.m3u8Path
    end

    def pause
      if @stream
        # Send SIGSTOP
        Process.kill("STOP", @stream)
      end
    end

    def resume
      if @stream
        # Send SIGCONT
        Process.kill("CONT", @stream)
      end
    end

    def kill
      if @stream
        # Send SIGKILL
        Process.kill("KILL", @stream)
      end
    end

    def m3u8Path
      ret = File.join("public", "streams")

      if not File.exists?(ret)
        Dir.mkdir(ret)
      end

      ret = File.join(ret, @uuid)
      if not File.exists?(ret)
        Dir.mkdir(ret)
      end

      ret
    end

    def m3u8StreamPath
      File.join(self.m3u8Path, "stream.m3u8")
    end

    def m3u8URL
      self.m3u8Path[6..-1]
    end

    def m3u8StreamURL
      self.m3u8StreamPath[6..-1]
    end

    def m3u8SegmentURL
      "/hls/segments/#{@uuid}/"
    end

    def ping(segmentFilename)
      segmentNumber = File.basename(segmentFilename, File.extname(segmentFilename))[8..-1].to_i
      segmentPath = File.join(self.m3u8Path, segmentFilename)

      if not File.exists?(segmentPath)
        return nil
      end

      # Delete last segment (which we assume will not be retrieved again)
      if segmentNumber > 1
        lastSegmentPath = File.join(self.m3u8Path, format("segment_%05d.ts", segmentNumber-2))
        File.delete(lastSegmentPath)
      end

      # Resume transcoder
      @segmentMutex.synchronize do
        @segmentsConsumed = [@segmentsConsumed, segmentNumber].max

        # always resume
        self.resume
      end

      segmentPath
    rescue
      nil
    end

    def m3u8_args
      ["-hls_segment_filename", File.join(self.m3u8Path, "segment_%05d.ts"), "-hls_time", @segmentSize.to_s, "-hls_list_size", @maxSegments.to_s, "-hls_base_url", self.m3u8SegmentURL, "-segment_list_flags", "live", self.m3u8StreamPath]
    end

    def start(seek = 0)
      return if @streaming
      @streaming = true

      Thread.new do
        command = ["ffmpeg", "-nostats", "-hide_banner", "-ss", seek.to_s, "-i", self.path] + self.m3u8_args
        Open3.popen3(*command) do |input, io, err, thread|
          @stream = thread.pid

          input.close

          begin
            loop do
              ready = IO.select([io, err], [], [], 60)
              if ready.nil?
                # Timeout... let's give up and purge this transcoder
                break
              end

              if ready
                readable = ready[0]

                # Throw out stdout/stderr
                # TODO: detect when next output file is generated and pause the stream aligned with segment duration
                #       allow the stream to continue and catch up if it misses a deadline
                readable.each do |pipe|
                  pipe.read_nonblock(1024)
                  segmentFileName = File.join(self.m3u8Path, format("segment_%05d.ts", @segmentsAvailable))
                  if File.exists?(segmentFileName)
                    @segmentMutex.synchronize do
                      @segmentsAvailable += 1
                      if @segmentsAvailable >= @segmentsConsumed + @maxSegments
                        # Pause
                        self.pause
                      end
                    end
                  end
                end
              end
            end
          rescue IOError => e
            puts "IOError: #{e}"
          rescue
            puts "oh no"
          end

          self.kill
        end

        @streaming = false
        @stream = nil
        self.purge
      end
    end

    def metadata
      if @metadata.nil?
        command = ["ffprobe", "-show_format", self.path]
        ret = {}

        data = StringIO.new("")

        p = Open3.popen3(*command) do |_, io, err, thread|
          _.close

          begin
            until (io.eof? && err.eof?) do
              ready = IO.select([io, err])
              if ready
                readable = ready[0]

                readable.each do |pipe|
                  if pipe == io
                    data << io.read_nonblock(1024)
                  elsif pipe == err
                    err.read_nonblock(1024)
                  end
                end
              end
            end
          rescue IOError => e
          end
        end

        data.rewind
        withinFormat = false
        data.each_line do |line|
          line = line.chop
          if line == "[/FORMAT]"
            withinFormat = false
          end
          if withinFormat
            key, value = line.split("=")
            key = key.strip
            if ["duration"].include? key
              ret[key.intern] = value.to_f
            end
          end
          if line == "[FORMAT]"
            withinFormat = true
          end
        end
        @metadata = ret
      end

      @metadata
    end

    def frame(options = {}, &block)
      options[:format] ||= :jpeg
      options[:start]  ||= 0
      options[:width]  ||= 512
      options[:height] ||= -1

      case options[:format]
      when :jpeg
        format = "mjpeg"
      else
        format = "mjpeg"
      end

      command = ["ffmpeg", "-ss", options[:start].to_s, "-i", self.path, "-vframes", "1", "-f", format, "-vf", "scale=#{options[:width]}:#{options[:height]}", "-"]

      p = Open3.popen3(*command) do |_, io, err, thread|
        _.close

        begin
          until (io.eof? && err.eof?) do
            ready = IO.select([io, err])
            if ready
              readable = ready[0]

              readable.each do |pipe|
                if pipe == io
                  yield(io.read_nonblock(1024))
                elsif pipe == err
                  err.read_nonblock(1024)
                end
              end
            end
          end
        rescue IOError => e
        end
      end
    end

    def read(start, length, &block)
      self.transcode

      position = start
      final = start + length
      bytesRead = 0

      until bytesRead >= length || @buffer.closed? do
        toWrite = nil
        @mutex.synchronize do
          while (@buffer.length <= position)
            @resource.signal
            @resource.wait(@mutex)
          end

          @buffer.rewind
          bytesToRead = 1024*16
          if position + bytesToRead > @buffer.length
            bytesToRead = @buffer.length - position
          end
          if bytesRead + bytesToRead > length
            bytesToRead = length - bytesRead
          end

          @buffer.seek(position, IO::SEEK_SET)
          toWrite = @buffer.read(bytesToRead)

          bytesRead += bytesToRead
          position += bytesToRead
        end

        yield(toWrite)
      end
    end

    def mp4_args
      ["-vcodec", "h264", "-acodec", "aac", "-strict", "experimental", "-preset", "ultrafast", "-movflags", "frag_keyframe+empty_moov+faststart", "-f", "mp4", "-"]
    end

    def ogg_args
      ["-vcodec", "libtheora", "-acodec", "libvorbis", "-f", "ogg", "-"]
    end

    def transcode(&block)
      Thread.new do
        p = Open3.popen3(*["ffmpeg", "-i", self.path] + self.ogg_args) do |_, io, err, thread|
          _.close

          begin
            until (io.eof? && err.eof?) do
              ready = IO.select([io, err])
              if ready
                readable = ready[0]

                readable.each do |pipe|
                  if pipe == io
                    yield(io.read_nonblock(1024))
                  elsif pipe == err
                    err.read_nonblock(1024)
                  end
                end
              end
            end
          rescue IOError => e
            puts "IOError: #{e}"
          end
        end
      end
    end
  end
end
