require_relative '../lib/application'

host = Application::Config.options["host"] || "0.0.0.0"
port = Application::Config.options["port"] || 9292

bind "tcp://#{host}:#{port}"
